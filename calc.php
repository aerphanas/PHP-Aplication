<div class="container">
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<form class="form form-top" action="" method="post" accept-charset="utf-8">
				<fieldset>
					<legend class="col-form-legend">Calculator</legend>
					<p><label class="sr-only">Data 1</label><input class="form form-control" type="text" name="data1" placeholder="Data 1" autocomplete="off"></p>
					<p><label class="sr-only">Data 2</label><input class="form form-control" type="text" name="data2" placeholder="Data 2" autocomplete="off"></p>
					<p><select class="form form-control" name="actionc">
						<option value="Additionc">Additionc</option>
						<option value="Subtraction">Subtraction</option>
						<option value="Multiplication">Multiplication</option>
						<option value="Division">Division</option>
						<option value="Modulus">Modulus</option>
						<option value="Exponentiation">Exponentiation</option>
					</select></p>
					<p><input class="btn btn-block btn-success" type="submit" name="hitung" value="equals"></p>
					<?php
					if (isset($_POST['hitung'])) {
						$data1 = $_POST['data1'];
						$data2 = $_POST['data2'];
						$actionc = $_POST['actionc'];
						switch ($actionc) {
							case 'Addition':
								$hasil = $data1 + $data2;
								break;
							case 'Subtraction':
								$hasil = $data1 - $data2;
								break;
							case 'Multiplication':
								$hasil = $data1 * $data2;
								break;
							case 'Division':
								$hasil = $data1 / $data2;
								break;
							case 'Modulus':
								$hasil = $data1 % $data2;
								break;
							case 'Exponentiation':
								$hasil = $data1 ** $data2;
								break;
						}
					}
					?>
					<p><input class="form form-control" type="text" name="hasil" value="Equals <?php echo isset($_POST['hitung'])?$hasil:""; ?>" disabled></p>
				</fieldset>
			</form>
		</div>
		<div class="col-sm-4"></div>
	</div>
</div>